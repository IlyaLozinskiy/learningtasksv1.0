package Cons;

interface Func<T, T2> {
    T2 call(T value);
}

