package Cons;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class Cons<T> {
    final T head;
    final Cons<T> tail;

    public Cons(T head) {
        this.head = head;
        this.tail = null;
    }

    public Cons(T head, Cons<T> tail) {
        this.head=head;
        this.tail=tail;
    }

    public Cons<T> concat(T value) {
        return new Cons<T>(value, this);
    }

    public void print() {
        if (tail != null) {
            System.out.print(head + ",");
            tail.print();
        } else {
            System.out.println(head);
        }
    }
// Метод map должен возвращать список, содержащий в себе
// результаты применения функции переданной в map к каждому элементу исходного списка.
//
    public <T2> Cons<T2> map(Func<T, T2> func) {
       if(this.tail!=null){
           return new Cons<T2>(func.call(this.head), this.tail.map(func));
       }
       return new Cons<>(func.call(this.head));
    }
    public static void main(String[] args) {
        Cons<Integer> cons =
                new Cons<>(0)
                        .concat(1)
                        .concat(2)
                        .concat(3)
                        .concat(4);
        cons.print();
        Cons<Integer> result = cons.map(new Func<Integer, Integer>() {
            @Override
            public Integer call(Integer value) {
                return value * 2;
            }
        });
        result.print();
    }
}