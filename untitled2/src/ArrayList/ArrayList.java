package ArrayList;

import jdk.nashorn.internal.codegen.ObjectClassGenerator;

public class ArrayList {
    private Object[] data = new Object[10];
    private int size = 0;
    public Object[] getData() {
        return data;
    }

    int getSize(){
        return size;
    }
    void printArr(){
        for (int i = 0; i <size ; i++) {
            System.out.print(this.data[i]+ " ");
        }
        System.out.println();
        System.out.println("size: " + this.size);
    }
    void printFullArr(){
        for (int i = 0; i <data.length ; i++) {
            System.out.print(this.data[i]+ " ");
        }
        System.out.println();
        System.out.println("size: " + this.size);
    }
    void expandArray(){
       Object[]tmp=new Object[(this.data.length*3)/2+1];
        for (int i = 0; i <data.length ; i++) {
            if(data[i]==null){
                break;
            }
                tmp[i]=this.data[i];
        }
        this.data=tmp;
    }
    Object getElem(int index){
        for (int i = 0; i <this.data.length ; i++) {
            if(i==index){
                System.out.println(data[i]);
                return data[i];
            }
        }
        return null;
    }
    void addLast(Object element){
        if(data.length>size){
            for (int i = 0; i <data.length ; i++) {
                if (data[i]==null){
                    data[i]=element;
                    size++;
                    break;
                }
            }
        }else{
            expandArray();
            for (int i = 0; i <data.length ; i++) {
                if (data[i]==null){
                    data[i]=element;
                    size++;
                    break;
                }
            }
        }
    }
    void add(Object element, int index){
        if(index>size-1){
            addLast(element);
        }else
        if(data.length>size){
            Object[]tmp=new Object[this.data.length-index];
            for (int k = 0;  k<tmp.length ; k++) {
                tmp[k]=data[k+index];
            }
            this.data[index]=element;
            for (int i = index+1; i <data.length ; i++) {
                this.data[i]=tmp[i-index-1];
            }
            size++;
        }else{
            expandArray();
            Object[]tmp=new Object[this.data.length-index];
            for (int k = 0;  k<tmp.length ; k++) {
                if(data[k+index]==null){
                    break;
                }
                tmp[k]=data[k+index];
            }
            this.data[index]=element;
            for (int i = index+1; i <data.length ; i++) {
                if(data[i+1]==null){
                    break;
                }
                this.data[i]=tmp[i-index-1];
            }
            size++;
        }
    }
    void remove (int index){
        Object[]tmp=new Object[this.data.length-index-1];
        for (int k = 0;  k<tmp.length ; k++) {
            tmp[k]=data[k+index+1];
        }
        this.data[index]=null;
        for (int i = index; i <data.length ; i++) {
            if(i==data.length-1){
                data[i]=null;
                break;
            }
            this.data[i]=tmp[i-index];
        }
        size--;
        trimToSize();
    }
    void trimToSize(){
        Object [] tmp = new Object[size];
        for (int i = 0; i <tmp.length ; i++) {
            tmp[i]=data[i];
        }
        data=tmp;
    }
}
