package LinkedList1;

public class Node1 <D> {
  private D data;
  private Node1 next;

    Node1(D data){
        this.data=data;
    }

    public D getData() {
        return data;
    }

    public void setData(D data) {
        this.data = data;
    }

    public Node1 getNext() {
        return next;
    }

    public void setNext(Node1 next) {
        this.next = next;
    }


}
