package LinkedList1;

public class DoubleNode<T> {
    T data;
    DoubleNode next;
    DoubleNode prev;
    DoubleNode(T data){
        this.data=data;
        this.next=null;
        this.prev=null;
    }
}
