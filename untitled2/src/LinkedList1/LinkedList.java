package LinkedList1;

public class LinkedList {
    private int size = 0;
    private Node1 head;
    private Node1 tail;

    LinkedList() {}

    public int getSize() {
        return size;
    }

    public void printList() {
        Node1 tmp = head;
        for (int i = 0; i < this.size; i++) {

            System.out.print(tmp.getData() + " ");
            tmp=tmp.getNext();
        }
        System.out.println();
    }

    public Node1 getElem(int index) {
        Node1 tmp = head;
        int iter = 0;
        while (tmp != tail) {
            if (index == iter) {
                return tmp;
            }
            tmp = tmp.getNext();
            iter++;
        }
        return tail;
    }

    public void addLast(Object obj) {
       Node1 item = new Node1(obj);
        if (head == null) {
            head=item;
            size++;
            tail=item;
        } else {
            tail.setNext(item);
            size++;
           tail=item;
        }
    }

    public void add(Object obj, int index) {
        Node1 item = new Node1(obj);
        if (index >= size) {
            addLast(item);
        } else if (index == 0) {
            Node1 tmp1 = getElem(0);
            head=item;
            item.setNext(tmp1);
            size++;
        } else {
            Node1 tmp = getElem(index);
            Node1 tmp2 = getElem(index - 1);
            tmp2.setNext(item);
            item.setNext(tmp);
            size++;
        }
    }

    public void remove(int index) {
        if (index == 0) {
            Node1 tmp = getElem(0);
           head=tmp.getNext();
            size--;
        } else if (index == size - 1) {
            Node1 tmp2 = getElem(size - 2);
            tmp2.setNext(null);
            tail=tmp2;
            size--;
        } else {
            Node1 tmp2 = getElem(index - 1);
            Node1 tmp3 = getElem(index + 1);
            tmp2.setNext(tmp3);
            size--;
        }
    }
}