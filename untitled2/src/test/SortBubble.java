package test;

public class SortBubble {
    public static void main(String[] args) {
        int[] sort = {5, 12, 1, 50, 3,152,-1000};
        sortBubble(sort);
        for (int i = 0; i <sort.length ; i++) {
            System.out.println(sort[i]);
        }
    }

    static int[] sortBubble(int[] sort) {
        for (int i = 0; i < sort.length-1; i++) {
            for (int k = i+1; k < sort.length; k++) {
                if (sort[i]>sort[k]) {
                    int tmp = sort[i];
                    sort[i]=sort[k];
                    sort[k]=tmp;
                }

            }
        }
        return null;
    }

}