package test;

public class SortInsertion {
    public static void main(String[] args) {
        int[] sort = {12,8,4,5,7,1};
        for (int i = 0; i <sort.length ; i++) {
            for (int j =i; j >0 ; j++) {
                if (sort[j]<sort[i]){
                    int tmp = sort[j];
                    sort[j]=sort[i];
                    sort[i]=tmp;
                }
            }
        }

        for (int i = 0; i <sort.length ; i++) {
            System.out.println(sort[i]);
        }
    }
}
