package test;

public class Book {
    private String name;
    private String author;
    private int pagesCount;
    private int price;

static String getAuthor(Book book){

    return book.author;
}
static void setAuthor (Book book, String author){
    if(author.equals("")){
        System.out.println("Error");
        System.exit(1);
    }
    book.author = author;
}
    static Book getInstance(String name, String author, int pagesCount, int price){
        if(author.equals("")){
            System.out.println("Error");
            System.exit(1);
        }
        Book book = new Book();
        book.name = name;
        book.author  = author;
        book.pagesCount = pagesCount;
        book.price  = price;
        return book;
    }
}
