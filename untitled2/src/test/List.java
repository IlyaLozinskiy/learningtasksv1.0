package test;

import java.util.Scanner;

public class List {
    private String Name;
    private String Surname;
    private int Phone;

    static List getList(String Name, String Surname, int Phone) {
        List list = new List();
        list.Name = Name;
        list.Surname = Surname;
        list.Phone = Phone;
        return list;
    }

    static void printList(List list) {
        System.out.println(list.Name + " " + list.Surname + " " + list.Phone);
    }

    static void searchForPhone(List[] list) {
        System.out.println();
        System.out.println("Введите телефон");
        Scanner q = new Scanner(System.in);
        String A = q.nextLine();
        int f = Integer.parseInt(A);
        boolean find = false;
        int found = 0;
        for (int i = 0; i < list.length; i++) {
            if (f == list[i].Phone) {
                find = true;
                found = i;
                break;
            }
        }
        if (find) {
            System.out.println(list[found].Name + " " + list[found].Surname);
        } else {
            System.out.println("Не найдено");
        }
    }

    static void searchForName(List[] list) {
        System.out.println();
        System.out.println("Введите Имя");
        Scanner q = new Scanner(System.in);
        String name = q.nextLine();
        System.out.println("Введите Фамилию");
        String surname = q.nextLine();
        boolean find = false;
        int found = 0;
        for (int i = 0; i < list.length; i++) {
            if (name.equals(list[i].Name)) {
                if (surname.equals(list[i].Surname))
                    find = true;
                found = i;
                break;
            }
        }
        if (find) {
            System.out.println("телефон:");
            System.out.println(list[found].Phone);
        } else {
            System.out.println("Не найдено");
        }
    }
}
