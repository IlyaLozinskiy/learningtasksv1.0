package test;

public class UsingComplex {
    public static void main(String[] args) {
        Complex complex1 = new Complex(-1,1);
        Complex complex2 = new Complex(2,-12);
        Complex sum = new Complex(0,0);
        Complex mult = new Complex(0,0);
        complex1.printComplex();
        complex2.printComplex();
        sum.getSumComplex(complex1,complex2).printComplex();
        mult.getMultComplex(complex1,complex2).printComplex();

    }

}
