package test;

public class UsingList {
    public static void main(String[] args) {
        int n = 5;
        List[] adressBook = new List[n];
        adressBook[0] = List.getList("Анна", "Артёмова", 7552222);
        adressBook[1] = List.getList("Ирина", "Кузнецова", 7552223);
        adressBook[2] = List.getList("Иван", "Васильев", 7552224);
        adressBook[3] = List.getList("Александр", "Алексеев", 7552225);
        adressBook[4] = List.getList("Александра", "Котова", 7552226);
        for (int i = 0; i < adressBook.length; i++) {
            List.printList(adressBook[i]);
        }
        List.searchForPhone(adressBook);
        List.searchForName(adressBook);
    }

}
