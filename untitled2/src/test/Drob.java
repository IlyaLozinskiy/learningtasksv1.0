package test;

public class Drob {
    int chislitel;
    int znamenatel;
    int celoe;

    Drob(int b, int c) {
        this.chislitel = b;
        this.znamenatel = c;
    }

    Drob(int a, int b, int c) {
        this.celoe = a;
        this.chislitel = b;
        this.znamenatel = c;


    }

    void printDrob() {
        System.out.println(this.chislitel + "/" + this.znamenatel);
    }

    void checkForInt() {
        if (this.celoe != 0) {
            if (this.celoe > 0) {
                this.chislitel = this.chislitel + (this.celoe * this.znamenatel);
                this.celoe = 0;
            }else{
                this.chislitel = (this.celoe * this.znamenatel-this.chislitel)  ;
                this.celoe = 0;
            }
        }
    }

    Drob plus(Drob drob1) {
        this.checkForInt();
        drob1.checkForInt();
        System.out.println("Сумма:");
        if (this.znamenatel == drob1.znamenatel) {
            Drob sum = new Drob( this.chislitel + drob1.chislitel, this.znamenatel);
            return sum;
        }
        int tmp = this.znamenatel;
        this.chislitel *= drob1.znamenatel;
        this.znamenatel *= drob1.znamenatel;
        drob1.chislitel *= tmp;
        Drob sum = new Drob( this.chislitel + drob1.chislitel,this.znamenatel);
        return sum;
    }

    Drob subtract(Drob drob1) {
        this.checkForInt();
        drob1.checkForInt();
        System.out.println("Разница:");
        if (this.znamenatel == drob1.znamenatel) {
            Drob sum = new Drob(this.chislitel - drob1.chislitel, this.znamenatel);
            return sum;
        }
        int tmp = this.znamenatel;
        this.chislitel *= drob1.znamenatel;
        this.znamenatel *= drob1.znamenatel;
        drob1.chislitel *= tmp;
        Drob sum =new Drob (this.chislitel - drob1.chislitel,this.znamenatel);
        return sum;
    }
    Drob multiply (Drob drob1){
        this.checkForInt();
        drob1.checkForInt();
        System.out.println("Произведение:");
        Drob mult = new Drob(this.chislitel*drob1.chislitel,this.znamenatel*drob1.znamenatel);
        return mult;
    }
    Drob divide (Drob drob1){
        this.checkForInt();
        drob1.checkForInt();
        System.out.println("Частное:");
        Drob divide = new Drob(this.chislitel* drob1.znamenatel,this.znamenatel*drob1.chislitel);
        return divide;
    }

    Drob equals() {
        if (this.chislitel == 0) {
            System.out.println(0);
            return this;
        }
        double ch = this.chislitel;
        double zn = this.znamenatel;
        double z = Math.abs(ch)/zn;
        if (z > 1) {
            this.celoe = this.chislitel / this.znamenatel;
            if (this.chislitel%this.znamenatel==0){
                System.out.println(this.celoe);
            return this;
            }
            this.chislitel %= this.znamenatel;
            if (this.celoe<0)this.chislitel=Math.abs(this.chislitel);
            System.out.print(this.celoe + ".");
            printDrob();
            return this;
        } else if (z < 1) {
            int tmp = this.gcd();
            this.chislitel /= tmp;
            this.znamenatel /= tmp;
            if (this.celoe != 0) System.out.print(this.celoe + ".");
            printDrob();
            return this;
        } else {
            this.celoe = 1;
            System.out.println(this.celoe);
            return this;
        }
    }

    public int gcd() {
        int a = this.znamenatel;
        int b = this.chislitel;
        while (b != 0) {
            int tmp = a % b;
            a = b;
            b = tmp;
        }
        return Math.abs(a);
    }
}
