package test;

public class  Complex {
    int a;
    int b;

    Complex(int a, int b) {
        this.a = a;
        this.b = b;
    }

    void printComplex() {
        System.out.print(this.a);
        if (this.b == 1) {
            System.out.println("+" + "i");
        } else if (this.b == -1) {
            System.out.println("-" + "i");
        } else if (this.b > 0) {
            System.out.println("+" + this.b + "i");
        } else {
            System.out.println(this.b + "i");
        }

    }

    Complex getSumComplex(Complex complex, Complex complex1) {
        System.out.println("Сумма:");
//        Complex sum = new Complex(0, 0);
        this.a = complex.a + complex1.a;
        this.b = complex.b + complex1.b;
        return this;
    }

     Complex getMultComplex(Complex complex, Complex complex1) {
        System.out.println("Произведение:");
        this.a = (complex.a * complex1.a) + (complex.b * complex1.b * -1);
        this.b = (complex.a * complex1.b) + (complex.b * complex1.a);
        return this;
    }
}


