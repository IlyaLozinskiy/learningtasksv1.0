package test;

import java.util.Scanner;

public class pryamougolnik {
    public static int Input() {
        Scanner q = new Scanner(System.in);
        String A = q.nextLine();
        int input = Integer.parseInt(A);
        return input;
    }

    public static void printLength(int number, int[] length) {
        for (int i = 0; i < number; i++) {
            if (number - 1 == i) {
                int lengthI = length[i];
                System.out.println("Длинна прямоугольника " + (i + 1) + " равна: " + lengthI);
            }
        }
    }

    public static void printWidth(int number, int[] width) {
        for (int i = 0; i < number; i++) {
            if (number - 1 == i) {
                int widthI = width[i];
                System.out.println("Ширина прямоугольника " + (i + 1) + " равна: " + widthI);
            }
        }
    }

    public static void printSquare(int number, int[] length, int[] width) {
        for (int i = 0; i < number; i++) {
            if (number - 1 == i) {
                int square = length[i] * width[i];
                System.out.println("Площадь прямоугольника " + (i + 1) + " равна: " + square);
            }
        }
    }

    public static void printPerimetr(int number, int[] length, int[] width) {
        for (int i = 0; i < number; i++) {
            if (number - 1 == i) {
                int perimetr = (length[i] + width[i]) * 2;
                System.out.println("Периметр прямоугольника " + (i + 1) + " равен: " + perimetr);
            }
        }
    }

    public static int getParametr() {
        System.out.println("Выберете параметр:");
        System.out.println();
        System.out.println("1.Длина");
        System.out.println("2.Ширина");
        System.out.println("3.Площадь");
        System.out.println("4.Периметр");
        int parametr = Input();
        return parametr;
    }

    public static void outPut(int count1, int[] length, int[] width) {
        for (int i = 0; i < 4; i++) {

            switch (getParametr()) {
                case 1:
                    printLength(count1, length);
                    break;
                case 2:
                    printWidth(count1, width);
                    break;
                case 3:
                    printSquare(count1, length, width);
                    break;
                case 4:
                    printPerimetr(count1, length, width);
                    break;
                    default:
                        break;
            }
            System.out.println();
        }
    }
//        Создать метод, принимающий номер прямоугольника и печатающий на экран его длину
//        Создать метод, принимающий номер прямоугольника и печатающий на экран его ширину
//        Создать метод, принимающий номер прямоугольника и печатающий на экран его площадь
//        Создать метод, принимающий номер прямоугольника и печатающий на экран его периметр

    public static void main(String[] args) {
        while (true) {
            System.out.println("Введите количество прямоугольников");
            int count = Input();
            int[] length = new int[count];
            int[] width = new int[count];
            for (int i = 0; i < count; i++) {
                System.out.println("Введите длину " + (i + 1) + " прямоугольника");
                length[i] = Input();
                System.out.println("Введите ширину " + (i + 1) + " прямоугольника");
                width[i] = Input();
                System.out.println("Прямоугольник " + (i + 1) + ": " + length[i] + " на " + width[i]);

            }
            System.out.println("Введите номер прямоугольника");
            int count1 = Input();
            outPut(count1, length, width);


        }


    }
}






