package test;

public class UsingDrob {
    public static void main(String[] args) {
       new Drob(-5,3,7).plus(new Drob(11,21)).equals();
       new Drob(1,5).subtract(new Drob(1,1,2)).equals();
       new Drob(-1,2,5).multiply(new Drob(1,2)).equals();
       new Drob(-5,3,7).divide(new Drob(11,21)).equals();
        new Drob(2,4).equals();
    }
}