package test1.standart_geometry;

public class UsingRectangles {
    public static void main(String[] args) {
        Square square = new Square(5);
        System.out.println(square.getWidth());
        System.out.println(square.getHeight());
        square.setWidth(10);
        System.out.println(square.getWidth());
        System.out.println(square.getHeight());
    }


    static double sumOfAreas(Rectangle[] rectangles) {
        double sum = 0;
        for (int i = 0; i < rectangles.length; i++) {
            sum += rectangles[i].getArea();
        }
        return sum;
    }

    static double minOfAreas(Rectangle[] rectangles) {
        double min = rectangles[0].getArea();
        for (int i = 0; i < rectangles.length; i++) {
            double area = rectangles[i].getArea();
            if (area < min) {
                min = area;
            }
        }
        return min;
    }

    static double maxOfAreas(Rectangle[] rectangles) {
        double max = rectangles[0].getArea();
        for (int i = 0; i < rectangles.length; i++) {
            double area = rectangles[i].getArea();
            if (area > max) {
                max = area;
            }
        }
        return max;
    }
}


