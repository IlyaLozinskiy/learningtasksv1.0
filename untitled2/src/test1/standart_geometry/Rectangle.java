package test1.standart_geometry;

public class Rectangle {
  private   double height;
    private double width;

    Rectangle(double heigth, double width){
        this.height=heigth;
        this.width=width;
    }
     double getArea(){
        return this.height * this.width;
    }
    double getHeight(){
        return this.height;
    }
    double getWidth(){
        return this.width;
    }
    void setWidth(double width){
        if(width<=0){
            System.err.println("error");
            System.exit(1);
        }
        this.width=width;
    }

    void setHeigth(double heigth){
        if(heigth<=0){
            System.err.println("error");
            System.exit(1);
        }
        this.height=heigth;
    }
}



