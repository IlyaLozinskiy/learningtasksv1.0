package test1.standart_geometry;

public class Main {
    public static void main(String[] args) {
        Mammal fox = new Fox();
        fox.eatingMilk();
        fox.say();
    }
}