package test1.Objected;

public class PowerCalculation extends CalculationSuper {
    PowerCalculation(double a, double b){
        this.a= a;
        this.b =b;
    }
    @Override
    double execute() {
        return Math.pow(this.a,this.b);
    }

    @Override
    void print() {
        System.out.println(this.a + "^"+this.b);
    }
}
