package test1.Objected;

import test1.Objected.AddCalculation;

public class Multiply extends CalculationSuper {

    Multiply(double a, double b){
        this.a= a;
        this.b =b;
    }    @Override
    double execute() {
        return this.a*this.b;
    }

    @Override
    void print() {
        System.out.println(this.a + " * " + this.b);
    }
}
