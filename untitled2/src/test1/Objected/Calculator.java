package test1.Objected;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Calculator extends JFrame {
    JButton b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15, b0, b16, b17;
    JLabel l1, l2, l3, l4, l5;
    JTextField t1, t2;
    String dft = ".";
    int after;
    int before;
    int act = 0;
    boolean res = false;
    eHandler handler = new eHandler();
    public String preResult(int act){
        int t = Integer.parseInt(t1.getText());
        int l = Integer.parseInt(l1.getText());
        switch (act){
            case 1:
             return Integer.toString(t*l);//*-1 /-2 --3 +-4
            case 2:
                if (t==0){
                    return "Деление на ноль невозможно";
                }else {
                    return Integer.toString(l/t);
                }
            case 3:
                return Integer.toString(l+t);
            case 4:
                return Integer.toString(l-t);
        }

        return null;
    }

    public Calculator(String s) {
        super(s);
        setLayout(new FlowLayout());
        b0 = new JButton("0");
        b1 = new JButton("1");
        b2 = new JButton("2");
        b3 = new JButton("3");
        b4 = new JButton("4");
        b5 = new JButton("5");
        b6 = new JButton("6");
        b7 = new JButton("7");
        b8 = new JButton("8");
        b9 = new JButton("9");
        b10 = new JButton("/");
        b11 = new JButton("*");
        b12 = new JButton("+");
        b13 = new JButton("-");
        b14 = new JButton(".");
        b15 = new JButton("      =      ");
        b16 = new JButton("C");
        b17 = new JButton("CE");
        t1 = new JTextField(dft, 20);
        l1 = new JLabel(" ");
        l2 = new JLabel(" ");
        l3 = new JLabel(" ");
        add(l1);
        add(l2);
        add(l3);
        add(t1);
        add(b7);
        b7.addActionListener(handler);
        add(b8);
        b8.addActionListener(handler);
        add(b9);
        b9.addActionListener(handler);
        add(b10);
        b10.addActionListener(handler);
        add(b11);
        b11.addActionListener(handler);
        add(b4);
        b4.addActionListener(handler);
        add(b5);
        b5.addActionListener(handler);
        add(b6);
        b6.addActionListener(handler);
        add(b12);
        b12.addActionListener(handler);
        add(b13);
        b13.addActionListener(handler);
        add(b1);
        b1.addActionListener(handler);
        add(b2);
        b2.addActionListener(handler);
        add(b3);
        b3.addActionListener(handler);
        add(b0);
        b0.addActionListener(handler);
        add(b14);
        b14.addActionListener(handler);
        add(b15);
        b15.addActionListener(handler);
        add(b16);
        b16.addActionListener(handler);
        add(b17);
        b17.addActionListener(handler);
    }

    public class eHandler implements ActionListener {
        public void actionPerformed(ActionEvent e) {
//            try {
            if (e.getSource() == b16) {
                t1.setText(dft);
                l1.setText(" ");
                l2.setText(" ");
                l3.setText(" ");
            }
            if (e.getSource() == b17) {
                t1.setText(dft);
            }
            if (e.getSource() == b0) {
                if (res==true)t1.setText(dft);
                if (t1.getText().equals(dft)||t1.getText().equals("0")) {
                    t1.setText("0");
                } else {
                    before = Integer.parseInt(t1.getText());
                    t1.setText(before + "0");
                }
                res = false;
            }
            if (e.getSource() == b1) {
                if (res==true)t1.setText(dft);
                if (t1.getText().equals(dft)||t1.getText().equals("0")) {
                    t1.setText("1");
                } else {
                    before = Integer.parseInt(t1.getText());
                    t1.setText(before + "1");
                }
                res = false;
            }
            if (e.getSource() == b2) {
                if (res==true)t1.setText(dft);
                if (t1.getText().equals(dft)||t1.getText().equals("0")) {
                    t1.setText("2");
                } else {
                    before = Integer.parseInt(t1.getText());
                    t1.setText(before + "2");
                }
                res = false;
            }
            if (e.getSource() == b3) {
                if (res==true)t1.setText(dft);
                if (t1.getText().equals(dft)||t1.getText().equals("0")) {
                    t1.setText("3");
                } else {
                    before = Integer.parseInt(t1.getText());
                    t1.setText(before + "3");
                }
                res = false;
            }
            if (e.getSource() == b4) {
                if (res==true)t1.setText(dft);
                if (t1.getText().equals(dft)||t1.getText().equals("0")) {
                    t1.setText("4");
                } else {
                    before = Integer.parseInt(t1.getText());
                    t1.setText(before + "4");
                }
                res=false;
            }
            if (e.getSource() == b5) {
                if (res==true)t1.setText(dft);
                if (t1.getText().equals(dft)||t1.getText().equals("0")) {
                    t1.setText("5");
                } else {
                    before = Integer.parseInt(t1.getText());
                    t1.setText(before + "5");
                }
                res = false;
            }
            if (e.getSource() == b6) {
                if (res==true)t1.setText(dft);
                if (t1.getText().equals(dft)||t1.getText().equals("0")) {
                    t1.setText("6");
                } else {
                    before = Integer.parseInt(t1.getText());
                    t1.setText(before + "6");
                }
                res = false;
            }
            if (e.getSource() == b7) {
                if (res==true)t1.setText(dft);
                if (t1.getText().equals(dft)||t1.getText().equals("0")) {
                    t1.setText("7");
                } else {
                    before = Integer.parseInt(t1.getText());
                    t1.setText(before + "7");
                }
                res=false;
            }
            if (e.getSource() == b8) {
                if (res==true)t1.setText(dft);
                if (t1.getText().equals(dft)||t1.getText().equals("0")) {
                    t1.setText("8");
                } else {
                    before = Integer.parseInt(t1.getText());
                    t1.setText(before + "8");
                }
                res=false;
            }
            if (e.getSource() == b9) {
                if (res==true)t1.setText(dft);
                if (t1.getText().equals(dft)||t1.getText().equals("0")) {
                    t1.setText("9");
                } else {
                    before = Integer.parseInt(t1.getText());
                    t1.setText(before + "9");
                }
                res=false;
            }
            if(e.getSource()==b10){
                if (act == 0) {
                    l1.setText(t1.getText());
                    l2.setText("/");
                    before = Integer.parseInt(t1.getText());
                    t1.setText(dft);
                }else{
                    l2.setText("/");
                    l1.setText(preResult(act));
                    t1.setText(dft);
                }
                act = 2;
            }
            if (e.getSource() == b11) {
                if (act == 0) {
                        l1.setText(t1.getText());
                        l2.setText("*");
                       before = Integer.parseInt(t1.getText());
                        t1.setText(dft);
                }else{
                    l2.setText("*");
                    l1.setText(preResult(act));
                    t1.setText(dft);
                }
                act = 1;
            }
            if (e.getSource() == b12) {
                if (act == 0) {
                    l1.setText(t1.getText());
                    l2.setText("+");
                    before = Integer.parseInt(t1.getText());
                    t1.setText(dft);
                }else{
                    l2.setText("+");
                    l1.setText(preResult(act));
                    t1.setText(dft);
                }
                act = 3;
            }
            if (e.getSource() == b13) {
                if (act == 0) {
                    l1.setText(t1.getText());
                    l2.setText("-");
                    before = Integer.parseInt(t1.getText());
                    t1.setText(dft);
                }else{
                    l2.setText("-");
                    l1.setText(preResult(act));
                    t1.setText(dft);
                }
                act = 4;
            }

            if (e.getSource()==b15) {
                if(act==0){
                    if (t1.getText().equals(dft)){
                        t1.setText(dft);
                    }else{
                        t1.setText(t1.getText());
                    }
                }else{
                    if(t1.getText().equals(dft)){
                        t1.setText(l1.getText());
                    }else {
                        t1.setText(preResult(act));
                    }
                }

                l1.setText(" ");
                l2.setText(" ");
                l3.setText(" ");
                act=0;
                res = true;
            }
//            } catch (Exception ex) {
//            }

        }

    }
}

// switch (act) {
//         case 1:
//         if(t1.getText().equals(dft)){
//         t1.setText(l1.getText());
//         }else {
//         after = Integer.parseInt(t1.getText());
//         int result = before * after;
//         String res = Integer.toString(result);
//         t1.setText(res);
//         }
//default:
//
//        break;
//        }