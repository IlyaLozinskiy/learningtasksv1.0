package test1.Objected;

import test1.Objected.AddCalculation;

public class SubtractCalculation extends CalculationSuper {
    SubtractCalculation(double a, double b){
        this.a= a;
        this.b =b;
    }
    @Override
    public double execute() {
        return this.a-this.b;
    }

    @Override
    public void print() {
        System.out.println(this.a+"-"+this.b);
    }
}

