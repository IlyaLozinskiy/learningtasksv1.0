package test1.Objected;

import jdk.internal.cmm.SystemResourcePressureImpl;
import test1.Objected.AddCalculation;
import test1.Objected.DivideCalculation;
import test1.Objected.Multiply;
import test1.Objected.SubtractCalculation;
import java.util.Scanner;
/*Улучшить задачу с калькулятором. Выделить абстрактный класс или интерфейс Calculation, с методами print и execute.
Наследовать все виды Calculation от него.
 Улучшить задачу с калькулятором.
Создать класс CalculationProvider с методом getCalculation,
который запрашивает информацию у пользователя и возвращает объект типа Calculation.
 В методе main создать объект CalculationProvider и вызывать метод getCalculation
 в цикле, пока пользователь не введет “exit”.
 */

public class CalculationProvider {
    CalculationSuper getCalculation() {
        double a = 0;
        double b = 0;
        String k;
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите выражение");
        k = scan.nextLine();
        if (k.indexOf(94)!=-1){
            a=Double.parseDouble(k.substring(0,k.indexOf(94)));
            b=Double.parseDouble(k.substring(k.indexOf(94)+1,k.length()));
            return new PowerCalculation(a,b);
        }
        if (k.indexOf(42) != -1) {
            a = Double.parseDouble(k.substring(0, k.indexOf(42)));
            b = Double.parseDouble(k.substring(k.indexOf(42) + 1, k.length()));
            return new Multiply(a, b);
        }
        if (k.indexOf(43) != -1) {
            a = Double.parseDouble(k.substring(0, k.indexOf(43)));
            b = Double.parseDouble(k.substring(k.indexOf(43) + 1, k.length()));
            return new AddCalculation(a, b);
        }

        if (k.indexOf(47) != -1) {
            a = Double.parseDouble(k.substring(0, k.indexOf(47)));
            b = Double.parseDouble(k.substring(k.indexOf(47) + 1, k.length()));
            return new DivideCalculation(a, b);
        }
        if (k.indexOf(45) != -1) {
            if(k.lastIndexOf(40)==k.lastIndexOf(45)-1) {
                a = Double.parseDouble(k.substring(0, (k.indexOf(40)-1)));
                b = Double.parseDouble(k.substring(k.indexOf(40) + 1, k.indexOf(41)));
                return new SubtractCalculation(a, b);
            }if(k.indexOf(45)!=k.lastIndexOf(45))
                a = Double.parseDouble(k.substring(0, k.lastIndexOf(45)));
                b = Double.parseDouble(k.substring(k.lastIndexOf(45) + 1, k.length()));
                return new SubtractCalculation(a, b);
            }

        return null;
    }

    public static void main(String[] args) {
        boolean exit = false;
        while (exit == false) {
            System.out.println(new CalculationProvider().getCalculation().execute());
            System.out.println("Чтобы, продолжить нажмите Enter. Чтобы выйти, введите Exit");
            Scanner ex = new Scanner(System.in);
            String e = ex.nextLine();
            if(e.equalsIgnoreCase("Exit")){
                exit = true;
            }
        }
    }
}
