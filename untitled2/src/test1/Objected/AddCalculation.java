package test1.Objected;
/*Создать класс AddCalculation, он будет представлять собой один шаг калькулятора для сложения двух чисел
Класс должен иметь конструктор, принимающий два значения типа double и сохранять их в поля
Метод execute, который будет вычислять сумму полей и возвращать значение
Метод print, который будет писать на экран “<первое поле> + <второе поле>”
Например “2 + 3”, если в конструкторе передали значения 2 и 3

Создать класс MultiplyCalculation, представляющий умножение. Класс должен наследоваться от AddCalculation

Создать класс SubtractCalculation, представляющий вычитание. Класс должен наследоваться от AddCalculation

Создать класс DivideCalculation, представляющий деление. Класс должен наследоваться от AddCalculation
*/
public class AddCalculation extends CalculationSuper {
    AddCalculation (double a, double b){
        this.a= a;
        this.b =b;
    }
    @Override
    public double execute() {
        return this.a+this.b;
    }

    @Override
    public void print() {
        System.out.println(this.a+"+"+this.b);
    }
}
