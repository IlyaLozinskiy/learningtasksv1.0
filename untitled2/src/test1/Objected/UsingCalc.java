package test1.Objected;

import test1.Objected.Calculator;

import javax.swing.*;

public class UsingCalc {
    public static void main(String[] args) {
        Calculator calc = new Calculator("Калькулятор");
        calc.setVisible(true);
        calc.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        calc.setSize(250,300);
        calc.setResizable(false);
        calc.setLocationRelativeTo(null);
    }
}
