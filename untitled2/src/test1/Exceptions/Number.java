package test1.Exceptions;

import java.util.Scanner;

public class Number{
    double a;
    public void setA()throws ZeroException, NotNumberException{
        Scanner sc = new Scanner(System.in);
        String scanned = sc.nextLine();
        try{
            a=Double.parseDouble(scanned);
            if (a==0){
                throw new ZeroException();
            }
        }catch (NumberFormatException e){
            throw new NotNumberException();
        }
    }
}
