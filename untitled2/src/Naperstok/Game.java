package Naperstok;

import static Naperstok.Position.FIRST;
import static Naperstok.Position.SECOND;
import static Naperstok.Position.THIRD;

/*
1) положить мяч
2) перемещать стаканчики
3) сравнить выбор игрока с правдой
4) добавить/убавить очки у игрока
 */
public class Game {
    Player player;
    Cup f = new Cup(FIRST);
    Cup s = new Cup(SECOND);
    Cup t = new Cup(THIRD);

    Game(Player player) {
        this.player = player;
    }

    void play() {
        for (int i = 0; i < player.points+1; i++) {
            s.hasBall = true;
            int a = 1;
            int b = 3;
            int position = a + (int) (Math.random() * b);
            switch (position) {
                case 1:
                    s.pos = FIRST;
                    break;
                case 2:
                    s.pos = SECOND;
                    break;
                case 3:
                    s.pos = THIRD;
                    break;
            }
            System.out.println(position);
            player.setMove();
            if (player.move == s.pos) {
                System.out.println("Правильно");
                player.points++;
                System.out.println("Количество очков:" + player.points);
                i=0;
            } else {
                System.out.println("Не правильно");
                player.points--;
                System.out.println("Количество очков:" + player.points);
                i=0;
            }
        }
        System.out.println("Game Over");
        System.exit(1);
    }
}
