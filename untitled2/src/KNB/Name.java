package KNB;

import javax.swing.*;
import java.awt.event.*;
import java.awt.event.TextListener;

import static com.sun.deploy.uitoolkit.ToolkitStore.dispose;

public class Name {

    private JPanel jpanel1;
    private JPanel Regim;
    private JButton игрокПротивIIButton;
    private JLabel ChooseCount;
    private JButton a2ИгрокаButton;
    private JPanel Names;
    private JTextField nameP1;
    private JTextField nameP2;
    private JLabel name2;
    public JButton Start;
    private JLabel name1;
    boolean visible = false;
    boolean visible1 = true;
    JFrame frame = new JFrame("КНБ");

    public Name() {
        Start.setEnabled(false);
        игрокПротивIIButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Names.setVisible(true);
                name2.setVisible(false);
                nameP2.setVisible(false);
            }
        });
        a2ИгрокаButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Names.setVisible(true);
                name2.setVisible(true);
                visible = true;
                name1.setText("Игрок1, введите имя");
                nameP2.setVisible(true);
            }
        });

        nameP1.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (visible == false) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER && nameP1.getText().length() > 0) {
                        Start.setEnabled(true);
                    }
                } else {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER && nameP1.getText().length() > 0 && nameP2.getText().length() > 0) {
                        Start.setEnabled(true);
                    }
                }
            }
        });
        nameP2.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER && nameP1.getText().length() > 0 && nameP2.getText().length() > 0) {
                    Start.setEnabled(true);
                }
            }
        });
        Start.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                JFrame frame1 = new JFrame("КНБ");

                frame1.setContentPane(new StartGame(new Human(), getPlayer()).jpanel1);
                frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame1.pack();
                frame1.setVisible(true);
                frame1.setLocationRelativeTo(null);
                frame1.setSize(600, 400);
                frame1.setResizable(true);
                visible1 = false;
                open();

            }
        });
    }

    private void onOK() throws Exception {
        // add your code here
        dispose();
    }

    void open() {
        this.frame.setContentPane(new Name().jpanel1);
        this.frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.frame.pack();
        this.frame.setLocationRelativeTo(null);
        this.frame.setSize(400, 300);
        this.frame.setResizable(true);
        if (visible1 == false) {
            frame.setVisible(false);
        } else {
            this.frame.setVisible(true);
        }

    }

    void close() {
        this.frame.setVisible(false);
    }

    Player getPlayer() {
        if (visible == false) {
            return new Computer();
        }
        return new Human();
    }

    String playersName(Player player, int i) {
        if (i == 1) return name1.getText();
        return name2.getText();
    }
}








