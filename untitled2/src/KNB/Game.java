package KNB;

import java.util.Scanner;

public class Game {

    Player player1;
    Player player2;

    Game(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    void play() {
         String Name1=this.player1.getName();
        String Name2 = this.player2.getName();
        lab1:
        while(true) {
            {
                int FirstMove = this.player1.getMove();
                int SecondMove = this.player2.getMove();
                switch (FirstMove) {
                    case 1:
                        System.out.println(Name1 + " показывает камень");
                        switch (SecondMove) {
                            case 1:
                                System.out.println(Name2 + " показывает камень");
                                System.out.println("Ничья");
                                System.out.println(Name1+ ": " + player1.getPoints());
                                System.out.println(Name2+ ": " + player2.getPoints());
                                break;
                            case 2:
                                System.out.println(Name2 + " показывает ножницы");
                                System.out.println("Победил " + Name1);
                                player2.addPoint();
                                System.out.println(Name1+ ": " + player1.getPoints());
                                System.out.println(Name2+ ": " + player2.getPoints());
                                break;
                            case 3:
                                System.out.println(Name2 + " показывает бумагу");
                                System.out.println("Победил " +Name2);
                                player1.addPoint();
                                System.out.println(Name1+ ": " + player1.getPoints());
                                System.out.println(Name2+ ": " + player2.getPoints());
                                break;
                        }
                        break;
                    case 2:
                        System.out.println(Name1 + " показывает ножницы");
                        switch (SecondMove) {
                            case 1:
                                System.out.println(Name2 + " показывает камень");
                                System.out.println("Победил " +Name2);
                                player2.addPoint();
                                System.out.println(Name1+ ": " + player1.getPoints());
                                System.out.println(Name2+ ": " + player2.getPoints());
                                break;
                            case 2:
                                System.out.println(Name2 + " показывает ножницы");
                                System.out.println("Ничья");
                                System.out.println(Name1+ ": " + player1.getPoints());
                                System.out.println(Name2+ ": " + player2.getPoints());
                                break;
                            case 3:
                                System.out.println(Name2 + " показывает бумагу");
                                System.out.println("Победил " +Name1);
                                player1.addPoint();
                                System.out.println(Name1+ ": " + player1.getPoints());
                                System.out.println(Name2+ ": " + player2.getPoints());
                                break;
                        }
                        break;
                    case 3:
                        System.out.println(Name1 + " показывает бумагу");
                        switch (SecondMove) {
                            case 1:
                                System.out.println(Name2 + " показывает камень");
                                System.out.println("Победил " + Name1);
                                player1.addPoint();
                                System.out.println(Name1+ ": " + player1.getPoints());
                                System.out.println(Name2+ ": " + player2.getPoints());
                                break;
                            case 2:
                                System.out.println(Name2 + " показывает ножницы");
                                System.out.println("Победил " + Name2);
                                player2.addPoint();
                                System.out.println(Name1+ ": " + player1.getPoints());
                                System.out.println(Name2+ ": " + player2.getPoints());
                                break;
                            case 3:
                                System.out.println(Name2 + " показывает бумагу");
                                System.out.println("Ничья");
                                System.out.println(Name1+ ": " + player1.getPoints());
                                System.out.println(Name2+ ": " + player2.getPoints());
                                break;
                        }
                        break;
                }
                System.out.println("Продолжить?");
                System.out.println("1 - Да");
                System.out.println("2 - Нет");
                Scanner scanner = new Scanner(System.in);
                String Ch = scanner.nextLine();
                int Choise = Integer.parseInt(Ch);
                if (Choise == 1) {
                    continue lab1;
                } else {
                    System.exit(0);
                }
            }
        }
    }
}