package KNB;

import java.util.Scanner;

public class Human implements Player {
    String Name;
    int Move;
    int Point;

    @Override
    public String getName() {
        System.out.println("Введите имя");
        return scan(this.Name);
    }

    @Override
    public int getMove() {
        System.out.println("Вы показываете...");
        return scan(this.Move);
    }

    @Override
    public int addPoint() {
      return this.Point++;
    }

    @Override
    public int getPoints() {
        return this.Point;
    }

    static String scan(String string) {
        Scanner scanner = new Scanner(System.in);
        string = scanner.nextLine();
        return string;
    }

    static int scan(int i) {
        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine();
        i = Integer.parseInt(string);
        return i;
    }
}
