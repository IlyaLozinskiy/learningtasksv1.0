package KNB;

public interface Player {
    String getName();
    int getMove();
    int addPoint();
    int getPoints();
}
