package Hash_Table;

import java.util.ArrayList;

public class HashTable {
    private int size;
    private ArrayList<Pare>[] hash;

    HashTable(int size) {
        this.size = size;
        hash = new ArrayList[size];
        for (int i = 0; i < size; i++) {
            hash[i] = new ArrayList<Pare>();
        }
    }

    public int hashFunction(String key) {
        int charCode =0;
        for (int i = 0; i <key.length(); i++) {
            charCode+=(int)key.charAt(i);
        }
        return charCode;
    }

    public void printTable() {
        for (int i = 0; i < hash.length; i++) {
            System.out.print(i + ": ");
            for (int j = 0; j < hash[i].size(); j++) {
                Pare tmp = hash[i].get(j);
                System.out.print(tmp.key + " - " + tmp.value + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public void put(Pare pare) {
        int index =hashFunction(pare.key) % hash.length;
        for (int i = 0; i <hash[index].size() ; i++) {
            if(hash[index].get(i).key.equals(pare.key)){
                hash[index].get(i).value=pare.value;
                return;
            }
        }
        hash[index].add(hash[index].size(), pare);

    }

    public String get(String key) {
        int index = hashFunction(key) % hash.length;
        for (int j = 0; j < hash[index].size(); j++) {
            Pare tmp = hash[index].get(j);
            if (tmp.key.equals(key)) {
                return tmp.value;
            }
        }
        return null;
    }

    public static void main(String[] args) {
        HashTable one = new HashTable(10);
        for (int i = 0; i < one.size; i++) {
            one.put(new Pare("pavel" + i, "qwerty" + i));
        }
        for (int i = 0; i < one.size; i++) {
            one.put(new Pare("ivan" + i, "qwerty123" + i));
        }
        one.printTable();
        System.out.println(one.get("ivan3"));
        System.out.println(one.get("pavel4"));
        one.put(new Pare("ivan0","asda"));
        one.printTable();
        one.put(new Pare("ivan00","asda"));
        one.printTable();
    }
}

