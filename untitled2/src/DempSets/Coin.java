package DempSets;

public class Coin {
    private String country;
    private int value;
    private int century;

    public Coin(String country, int value, int century) {
        this.country = country;
        this.value = value;
        this.century = century;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        Coin coin = (Coin) o;
        if (century != coin.century) return false;
        if (value != coin.value) return false;
        return  country.equals(coin.country);
    }

    @Override
    public int hashCode() {
        return century;
    }
}
