package com.codebind;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class APp {
    private JButton buttonMSG;
    private JPanel panelMain;
    private JTextPane textPane1;

    public APp() {
        buttonMSG.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null,"Hello World");
            }
        });
    }
    JFrame frame = new JFrame("App");
    public void open(){
        frame.setContentPane(new APp().panelMain );
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
