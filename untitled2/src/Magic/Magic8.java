package Magic;

public enum Magic8 {
    MAYBE,
    NO,
    YES,
    LATER,
    SOON,
    NEVER
}
