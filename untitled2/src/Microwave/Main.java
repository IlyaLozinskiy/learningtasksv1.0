package Microwave;

public class Main {
    public static void main(String[] args) {
        Microwave phillips = new Microwave();
        Food pie = new Food();
        phillips.put(pie);
        phillips.close();
        try{
           phillips.cook();
            System.out.println("*Дзинь-Дзинь* Садитесь жрать, пожалуйста");
        }catch(HasNoFood e){
            System.out.println("Положите еду!");
        }catch (DoorIsOpen e){
            System.out.println("Дверь открыта!");
        }
    }
}
