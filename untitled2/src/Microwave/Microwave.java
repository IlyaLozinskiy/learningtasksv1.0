package Microwave;
public class Microwave {
    boolean isOpen = true;
    boolean hasFood=false;
    Food food = null;
    void open(){
        this.isOpen=true;
    }
    void close(){
        this.isOpen=false;
    }
    void put(Food food){
        this.food=food;
        this.hasFood=true;
    }
    void take(){
        this.food = null;
        this.hasFood=false;
    }
    void cook() throws DoorIsOpen, HasNoFood {
       try{
           if(isOpen){
               throw new DoorIsOpen();
           }
           food.isCoocked=true;
       }catch(NullPointerException e){
           throw new HasNoFood();
       }

    }
}
