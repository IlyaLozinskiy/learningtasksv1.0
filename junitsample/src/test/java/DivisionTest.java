import org.junit.Assert.*;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import static org.junit.Assert.assertTrue;

public class DivisionTest {
    @Test
    public void testDivideSimple() {
        Division division = new Division();
        double result = division.divide(4, 2);
        assertTrue(2 == result);
    }
    @Test
    public void testInput() {
        Scanner sc = new Scanner("431");
        Division division = new Division();
        int result = division.userInput(sc);
        assertTrue(432==result);
    }
}
