import org.omg.CORBA.portable.InputStream;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class Page {
    private final String address;

    public Page(String address){
        this.address = address;
    }

    public String getSource() throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        URL url = new URL(this.address);
        InputStream inputStream = (InputStream) url.getContent();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        while (reader.ready()){
            stringBuilder.append(reader.readLine());
        }
        return stringBuilder.toString();
    }
}

 class Main {
    public static void main(String[] args) throws IOException {
        System.out.println(new Page("http://habrahabr.ru/").getSource());
    }


}
