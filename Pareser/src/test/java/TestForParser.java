import org.junit.Test;

import java.util.ArrayList;

import  static  org.junit.Assert.*;
public class TestForParser {
    @Test
    public void test1() {
        Parser par = new Parser("<a href=\"https://habrahabr.ru/company/zfort/blog/339630/\" class=\"post__title_link\">PHP-Дайджест № 118 – свежие новости, материалы и инструменты (24 сентября – 9 октября 2017)</a>");
        ArrayList<String> headers = par.parse();
        String expected = "PHP-Дайджест № 118 – свежие новости, материалы и инструменты (24 сентября – 9 октября 2017)";
        String actual = headers.get(0);
        assertTrue(expected.equals(actual));
    }

@Test
    public void test2() {
        Parser par = new Parser("<ul class=\"post__marks inline-list\"></ul><a href=\"https://habrahabr.ru/company/zfort/blog/339630/\" class=\"post__title_link\">PHP-Дайджест № 118 – свежие новости, материалы и инструменты (24 сентября – 9 октября 2017)</a><a href=\"https://habrahabr.ru/company/ods/blog/339094/\" class=\"post__title_link\">Ежемесячная руЕжемесячная рубрика «Читаем статьи за вас». Сентябрь 2017брика «Читаем статьи за вас». Сентябрь 2017</a>");
        ArrayList<String> headers = par.parse();
        String expected = "Ежемесячная руЕжемесячная рубрика «Читаем статьи за вас». Сентябрь 2017брика «Читаем статьи за вас». Сентябрь 2017";
        String actual = headers.get(1);
        assertTrue(expected.equals(actual));
    }
}
