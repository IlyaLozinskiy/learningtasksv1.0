/**
 * Created by mikle on 10/16/17.
 */
public class ArrayList implements List {
    private int[] elements;
    private int count;

    public ArrayList() {
        elements = new int[10];
        count = 0;
    }

    public int get(int index) {
        return elements[index];
    }

    public void addLast(int newNumber) {
        elements[count] = newNumber;
        count += 1;
    }

    public void add(int index, int newNumber) {

    }

    public void remove(int index) {

    }

    public int count() {
        return 0;
    }

    public boolean isEmpty() {
        return false;
    }
}
