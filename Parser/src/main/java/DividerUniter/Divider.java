package DividerUniter;

import java.io.*;
import java.nio.file.Path;
import java.util.ArrayList;

public class Divider {
    int count;
    Path way;

    Divider(int count, Path way) {
        this.count = count;
        this.way = way;
    }

    public ArrayList<Integer> read() throws IOException {
        FileInputStream in = new FileInputStream(way.toString());
        ArrayList<Integer> arr = new ArrayList<Integer>();
        while (true) {
            int b = in.read();
            if (b == -1) {
                break;
            }
            arr.add(b);
        }
        in.close();
        return arr;
    }

    public void divide() throws IOException {
        ArrayList<Integer> arr = read();
        int breakPoint = arr.size() / count;
        for (int i = 0; i < count; i++) {
            FileOutputStream o = new FileOutputStream(way.getParent().toString()+ String.valueOf(i) +".txt");
            for (int j = i * breakPoint; j < (i + 1) * breakPoint; j++) {
                if (i == count - 1) {
                    for (int k = i * breakPoint; k < arr.size(); k++) {
                        o.write(arr.get(k));
                        System.out.print(arr.get(k) + " ");

                    }
                    o.flush();
                    o.close();
                    break;
                }
                o.write(arr.get(j));
                System.out.print(arr.get(j) + " ");
            }
            System.out.println();
        }
    }
}


