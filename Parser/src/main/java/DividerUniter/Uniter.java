package DividerUniter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class Uniter {
    int count;

    Uniter(int count) {
        this.count = count;
    }

    public FileInputStream getWay(int way) throws FileNotFoundException {
        return new FileInputStream("C:\\Users\\РС\\Desktop\\Сборщик Файлов\\" + String.valueOf(way) + ".txt");
    }

    public ArrayList<Integer> read() throws IOException {
        ArrayList<Integer> arr = new ArrayList<Integer>();
        for (int i = 0; i < count; i++) {
            FileInputStream in = getWay(i);
            while (true) {
                int b = in.read();
                if (b == -1) {
                    in.close();
                    break;
                }
                arr.add(b);
            }
        }
        return arr;
    }

    public void unite() throws IOException {
        FileOutputStream out = new FileOutputStream("C:\\Users\\РС\\Desktop\\Сборщик Файлов\\Unified.txt");
        ArrayList<Integer> arr = read();
        for (int i = 0; i <arr.size(); i++) {
            out.write(arr.get(i));
        }
        out.flush();
        out.close();
    }
}
