import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import static org.junit.Assert.*;
public class CryptingWrapperTest {
   @Test
    public void testCrypt() throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        CryptingWrapper wrapper = new CryptingWrapper(out);

        wrapper.write(4);
        byte[] crypted = out.toByteArray();
        byte [] expected = {5};
        assertArrayEquals(expected, crypted);

    }
}


