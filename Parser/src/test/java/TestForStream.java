import org.junit.Test;

import java.io.*;
import java.util.Arrays;
import static org.junit.Assert.*;

public class TestForStream {
    @Test
    public void cryptTest() throws IOException {
        byte[]original ={1,2,3,4,5};
        ByteArrayInputStream input = new ByteArrayInputStream(original);
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        Text.crypt(input, output);
        byte[]crypted = output.toByteArray();
        input.close();
        output.close();
        byte[] expected = {2,3,4,5,6};
        assertArrayEquals(expected,crypted);
    }
    @Test
    public void decryptTest() throws IOException {
        byte[] crypted = {2,3,4,5,6};
        InputStream input = new ByteArrayInputStream(crypted);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Text.decrypt(input,out);
        byte[] decrypted = out.toByteArray();
        input.close();
        out.close();
        byte[] expected = {1,2,3,4,5};
        assertArrayEquals(expected,decrypted);
    }
}

