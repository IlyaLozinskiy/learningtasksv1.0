import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static org.junit.Assert.assertArrayEquals;

public class DecryptingWrappeTest {
    @Test
    public void testCrypt() throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        decryptingWrapper wrapper = new decryptingWrapper(out);

        wrapper.write(5);
        byte[] crypted = out.toByteArray();
        byte [] expected = {4};
        assertArrayEquals(expected, crypted);

    }
}
