package DempSets;

import com.neovisionaries.i18n.CountryCode;

public class Coin {
    private CountryCode country;
    private int value;
    private int century;

    public Coin(String country, int value, int century) {
        this.country = CountryCode.getByCode(country);
        this.value = value;
        this.century = century;
    }

    public CountryCode  getCountry() {
        return country;
    }

    public int getValue() {
        return value;
    }

    public int getCentury() {
        return century;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
//        if(this.hashCode()!=o.hashCode()) return false;
        Coin coin = (Coin) o;
        if(this.century!=((Coin) o).century)return false;
        if (value != coin.value) return false;
        return  country.equals(coin.country);
    }

    @Override
    public int hashCode() {
        return century;
    }
}
