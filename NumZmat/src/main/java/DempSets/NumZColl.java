package DempSets;

import com.neovisionaries.i18n.CountryCode;

import java.util.ArrayList;

public class NumZColl {
    ArrayList[][] coins = new ArrayList[20][20];

    NumZColl() {
        for (int i = 0; i < coins.length; i++) {
            for (int j = 0; j < coins[i].length; j++) {
                coins[i][j] = new ArrayList();
            }
        }
    }

    public void print() {
        for (int i = 0; i < coins.length; i++) {
            for (int j = 0; j < coins[i].length; j++) {
                for (int k = 0; k < coins[i][j].size(); k++) {
                    Coin coin = (Coin) coins[i][j].get(k);
                    System.out.print(coin.getCountry().getName() + " - " + coin.getCentury() + "th century, - " + coin.getValue()+ " , ");
                }
//                System.out.println();
            }
            System.out.println();
        }
    }

    public void put(Coin coin) {
        if (isInSet(coin) == true) return;
        coins[coin.hashCode()][coin.getCountry().getNumeric()%20].add(coin);
    }

    public void remove(Coin coin) {
        if (isInSet(coin) == false) return;
        coins[coin.hashCode()][coin.getCountry().getNumeric()%20].remove(coin);
    }

    public boolean isInSet(Coin coin) {
        for (int i = 0; i < coins[coin.hashCode()][coin.getCountry().getNumeric()%20].size(); i++) {
            if (coins[coin.hashCode()][coin.getCountry().getNumeric()%20].get(i).equals(coin)) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        NumZColl one = new NumZColl();
        for (int i = 0; i < 20; i++) {
            one.put(new Coin("UA", 10, i));
        }
        for (int i = 0; i < 20; i++) {
            one.put(new Coin("JP", 10, i));
        }
        for (int i = 0; i < 20; i++) {
            one.put(new Coin("RU", 10, i));
        }
        for (int i = 0; i < 20; i++) {
            one.put(new Coin("UA", i, i));
        }
        one.print();
    }
}

