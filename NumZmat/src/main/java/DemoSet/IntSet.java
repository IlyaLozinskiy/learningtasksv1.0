package DemoSet;


import java.util.ArrayList;

public class IntSet {
    ArrayList[] set = new ArrayList[10];

    IntSet() {
        for (int i = 0; i < set.length; i++) {
            set[i] = new ArrayList();
        }
    }
    public IntSet copySet(){
        IntSet result = new IntSet();
        for (int i = 0; i <this.set.length ; i++) {
            for (int j = 0; j <this.set[i].size(); j++) {
                result.put((Integer) this.set[i].get(j),false);
            }
        }
        return result;
    }

    public void put(int el) {
        if (isInSet(el) == true) return;
        set[el % set.length].add(el);
    }
    public void put(int el, boolean needCheck) {
        set[el % set.length].add(el);
    }

    public void remove(int el) {
        if (isInSet(el) == false) return;
        Object o = (Integer) el;
        set[(el % set.length)].remove(o);
    }

    public boolean isInSet(int el) {
        for (int i = 0; i < set[el % set.length].size(); i++) {
            if ((Integer) set[el % set.length].get(i) == el) {
                return true;
            }
        }
        return false;
    }

    public void print() {
        for (int i = 0; i < 10; i++) {
            System.out.print(i + ": ");
            for (int j = 0; j < set[i].size(); j++) {
                System.out.print(set[i].get(j) + " ");
            }
            System.out.println();
        }
    }

    public IntSet crossSet(IntSet s) {
        IntSet result = new IntSet();
        for (int i = 0; i < this.set.length; i++) {
            for (int j = 0; j < set[i].size(); j++) {
                if (s.isInSet((Integer) set[i].get(j)) == true) {
                    result.put((Integer) set[i].get(j));
                }
            }
        }
        return result;
    }

    public IntSet unitSet(IntSet s) {
        IntSet result = this.copySet();
        for (int i = 0; i < s.set.length; i++) {
            for (int j = 0; j < s.set[i].size(); j++) {
                if (this.isInSet((Integer) s.set[i].get(j)) == false) {
                    result.put((Integer) s.set[i].get(j));
                }
            }
        }
        return result;
    }
    public IntSet subtractSet(IntSet s){
        IntSet cross = this.crossSet(s);
        IntSet result = this.copySet();
        for (int i = 0; i <set.length ; i++) {
            for (int j = 0; j < set[i].size(); j++) {
                if (cross.isInSet((Integer) set[i].get(j)) == true) {
                    result.remove((Integer) set[i].get(j));
                }
            }
        }
        return result;
    }
//    public IntSet inverse(){
//
//    }

    public static void main(String[] args) {
        IntSet one = new IntSet();
        for (int i = 0; i < 100; i++) {
            one.put(i);
        }
        System.out.println("One");
        one.print();
        IntSet two = new IntSet();
        for (int i = 85; i <110; i++) {
            two.put(i);
        }
        System.out.println("Two");
        two.print();
        System.out.println("Cross");
        one.crossSet(two).print();
        System.out.println("Unit");
        one.unitSet(two).print();
        System.out.println("Subtract");
        one.subtractSet(two).print();
        System.out.println("Subtract");
        two.subtractSet(one).print();
    }
}
