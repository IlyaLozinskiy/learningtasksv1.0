import java.util.LinkedList;

public class Stack<T> {
   private LinkedList<T> list;
    Stack(){
       this.list=new LinkedList<>();
    }

    public void addElement(T element){
        list.addLast(element);
    }
    public void deleteElement(){
        list.removeLast();
    }
    public T getTop (){
       return list.getLast();
    }
    public boolean isEmpty(){
        return list.isEmpty();
    }

    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        stack.addElement(13);
        stack.addElement(52);
        stack.addElement(144);
        stack.addElement(556);

        System.out.println(stack.getTop());
        stack.deleteElement();
        System.out.println(stack.getTop());

    }
}
