package Box;

public class JenBox<TContent> {
    private TContent content;

    public JenBox(TContent content) {
        this.content = content;
    }

    public JenBox() {

    }

    public TContent get() {
        return content;
    }

    public void put(TContent content) {
        this.content = content;
    }
}
