package Box;

public class Main {
    public static void main(String[] args) {
        PhoneModel n = new PhoneModel("Nokia", 500);
        JenBox<String> box = new JenBox<String>();
        box.put("qweqweqw");
        System.out.println(box.get());
        JenBox<PhoneModel> box2 = new JenBox<PhoneModel>();
        box2.put(n);
        PhoneModel s = box2.get();
        System.out.println(box2.get().getName());
        printANYTH(123);
        printANYTH("aff");
        printANYTH(n);
    }

    public static void print(JenBox<?> box) {
        System.out.println("[" + box.get() + "]");
    }

    public static <T> void printANYTH(T parametr){
        System.out.println(parametr);
    }
}
