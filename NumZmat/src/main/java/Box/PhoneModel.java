package Box;

public class PhoneModel {
    String name;
    int price;

    public PhoneModel(String name, int price) {
        this.name = name;
        this.price = price;
    }
    public PhoneModel() {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
