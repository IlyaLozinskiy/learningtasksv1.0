import org.junit.Test;

import static org.junit.Assert.*;

/*Дан массив из n цифр.
        Задача: за максимум 2 прохода по массиву вывести на экран все цифры в порядке возрастания.

        Например, для массива { 5, 1, 4, 1, 5, 4, 3, 4 } вывести на экран:
        1
        1
        3
        4
        4
        4
        5
        5
*/
public class SortingTest {
    @Test
    public void DefaultTest() {
        int[] test = {5, 1, 4, 1, 5, 4, 3, 4};
        int[] actual = new SmartSorting().sortSmartly(test);
        int[] expected = {1, 1, 3, 4, 4, 4, 5, 5};
        assertArrayEquals(expected,actual);

    }

}
