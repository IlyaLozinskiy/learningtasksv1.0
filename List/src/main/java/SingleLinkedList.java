import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by mikle on 10/15/17.
 */
public class SingleLinkedList<TElement> /*implements List*/ {
    private SingleLinkedElement<TElement> head;

    private SingleLinkedElement<TElement> findLast(){
        SingleLinkedElement result = head;
        while (result.next != null){
            result = result.next;
        }
        return result;
    }

    private void newNext(SingleLinkedElement <TElement> element, TElement newData){
        SingleLinkedElement <TElement> newElement = new SingleLinkedElement <TElement>(newData);
        element.next = newElement;
    }

    public SingleLinkedList() {
        this.head = null;
    }

    public TElement get(int index) {
        if(index<0||index>=count()){
            throw new IndexOutOfBoundsException();
        }
        SingleLinkedElement <TElement>current = head;
        int currentIndex = 0;
        while(currentIndex != index){
            currentIndex += 1;
            current = current.next;
        }
        return current.data;
    }

    public void addLast(TElement newNumber) {
        if(isEmpty()){
            head = new SingleLinkedElement(newNumber);
            return;
        }

        SingleLinkedElement last = findLast();
        newNext(last, newNumber);
    }

    public void add(int index, int newNumber) {
        SingleLinkedElement current = head;
        int currentIndex = 0;

        if (index < 0 || index > count()){
            throw new IndexOutOfBoundsException();
        }

        if (index == 0){
            SingleLinkedElement newElement = new SingleLinkedElement(newNumber);
            newElement.next = head;
            head = newElement;
            return;
        }

        while (currentIndex != index - 1) {
            current = current.next;
            currentIndex += 1;
        }

        SingleLinkedElement newElement = new SingleLinkedElement(newNumber);
        newElement.next = current.next;
        current.next = newElement;
    }

    public void remove(int index) {
        SingleLinkedElement current = head;
        SingleLinkedElement previous = null;
        int currentIndex = 0;

        if (isEmpty() || index < 0 || index >= count()){
            throw new IndexOutOfBoundsException();
        }

        if (index == 0) {
            head = head.next;
            return;
        }

        while (currentIndex != index){
            previous = current;
            current = current.next;
            currentIndex += 1;
        }

        previous.next = current.next;
    }

    public int count() {
        if(isEmpty()) {
            return 0;
        }

        int result = 1;
        SingleLinkedElement current = head;

        while (current.next != null){
            result += 1;
            current = current.next;
        }

        return result;
    }

    public boolean isEmpty() {
        return head == null;
    }

    public static void main(String[] args) {
        List<String> list = new LinkedList<>();
        List<String> arr = new ArrayList<>();
        list.add("qqq");
        list.get(0);
    }
}
