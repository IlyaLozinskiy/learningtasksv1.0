import java.util.Arrays;

/**
 * Created by mikle on 10/16/17.
 */
public class ArrayList implements List {
    private int[] elements;
    private int count;

    public ArrayList() {
        elements = new int[10];
        count = 0;
    }

    public int get(int index) {
        return elements[index];
    }

    public void addLast(int newNumber) {
        extendArray();
        elements[count] = newNumber;
        count += 1;
    }

    public void add(int index, int newNumber) {
        extendArray();
        for (int i = count; i > index ; i--) {
            elements[i]=elements[i-1];
        }
        elements[index]=newNumber;
        count+=1;
    }

    public void remove(int index) {
        if(index<0||index>count||isEmpty()){
            throw new IndexOutOfBoundsException();
        }
        for (int i = index; i <count-1 ; i++) {
           elements[i]=elements[i+1];
        }
        count-=1;
    }

    public int count() {
        return count;
    }

    public boolean isEmpty() {
       return count==0;
    }

    public void extendArray() {
        if (count == elements.length) {
            Arrays.copyOf(elements,elements.length*2);
        } else {return;}
    }
    public void printArray() {
        for (int i = 0; i <elements.length ; i++) {
            System.out.print(elements[i]+ " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        ArrayList arr = new ArrayList();
        for (int i = 0; i <10 ; i++) {
            arr.add(i,i);
        }
        arr.printArray();
        arr.remove(1);
        arr.printArray();
        arr.add(1,54);
        arr.printArray();
        arr.remove(arr.count);
        arr.printArray();
        arr.remove(0);
        arr.printArray();

    }
}
