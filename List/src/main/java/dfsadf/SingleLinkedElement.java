package dfsadf;

/**
 * Created by mikle on 10/15/17.
 */
public class SingleLinkedElement {
    SingleLinkedElement next;
    Object data;

    public SingleLinkedElement(Object newData) {
        data = newData;
    }

    @Override
    public String toString() {
        return "SingleLinkedElement{data="+ data +'}';
    }
}
