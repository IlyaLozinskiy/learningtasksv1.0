package dfsadf;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        SingleLinkedList list = new SingleLinkedList();
        list.addLast("Hello!");
        list.addLast(500);
        list.addLast(new Scanner(System.in));
        list.addLast(null);
        list.addLast(new SingleLinkedList());

        String obj = (String) list.get(0);
        System.out.println(obj.toUpperCase());
    }
}
