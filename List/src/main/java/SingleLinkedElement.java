/**
 * Created by mikle on 10/15/17.
 */
public class SingleLinkedElement<T> {
    SingleLinkedElement<T> next;
    T data;

    public SingleLinkedElement(T newData) {
        data = newData;
    }

    @Override
    public String toString() {
        return "SingleLinkedElement{data="+ data +'}';
    }
}
